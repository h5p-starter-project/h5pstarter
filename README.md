# H5P Starter Project - Theme

This is a Bootstrap Subtheme built for the H5P Starter Project (Drupal 8).

It is built with the CDN starterkit available from the Drupal Bootstrap project.

* Drupal Bootstrap: https://www.drupal.org/project/bootstrap
* Bootstrap Framework: https://getbootstrap.com/docs/3.4/
* jsDelivr CDN: http://www.jsdelivr.com
