(function ($, Drupal, drupalSettings) {

    'use strict';
  
    Drupal.behaviors.formScripts = {
      attach: function (context, settings) {

        // move node edit/create form buttons to form-right region
        //$(".form-right").prepend( $("div#edit-actions") );
        
        // move form actions region to the correct region
        $(".form-actions-append").append( $("div#edit-actions") );
        $(".form-actions-prepend").prepend( $("div#edit-actions") );

        $('#start-h5p-tour').click(function(){
          Tour.run([
            {
              element: $('a[href="/node/add/interactive_content"].is-active'),
              content: 'Welcome to the H5P interactive content creator!',
              position: 'bottom'
            },
            {
              element: $('#edit-title-wrapper'),
              content: `
              <h2>Start by providing an Administrative Title for your content</h2>
              <p>This Title will be used when displaying your content within this site. Make it descriptive enough for others to easily identify the purpose of your content.</p>
              <p class="small text-muted">Example: "Developing a Research Question (for Grad Students)"</p>`,
              position: 'right',
              padding: 15
            },
            {
              element: $('#edit-field-subject-wrapper'),
              content: `
              <h2>What topic or subject does your content cover?</h2>
              <p>Select one of the pre-defined subject areas that your content covers. This will help keep all of the content on this site organized and easy to browse through.</p>
              <p>If your subject is not listed, you can request that it be added using this <a href="/contact/suggest_a_subject_or_topicform" target="_blank">form</a>.</p>`,
              position: 'right',
              padding: 15
            },
            {
              element: $('#edit-field-instructor-description-wrapper'),
              content: `<h2>What is the goal, intended use, or specifications of this content?</h2>
              <p>Providing a description of your content will help others understand your intended goals, the processes you used to create it, how it should be implmented, etc.</p>
              <p class="small text-muted">Example: "This slideshow presents Graduate Students with an brief primer on developing Research Questions for their thesis work.</p>`,
              position: 'right',
              padding: 15
            },
            {
              element: $('#edit-field-tags-wrapper'),
              content: `<h2>Group your content using tags</h2>
              <p>You can add Tags to your to create groups and lists of your content. Tags also help keep your content organized for yourself. You can add multiple tags by separating them with commas.</p>
              <p class="small text-muted">Example: Example, CRSCODE 403, Instructors Name, Colour</p>
              <p><a href="/tags/examples" target="_blank">View a tag landing page</a>.</p>`,
              position: 'right',
              padding: 15
            },
            {
              element: $('#edit-field-work-in-progress-wrapper'),
              content: `<h2>Options: Work in Progress (WIP)</h2>
              <p>Keep this option Enabled if this content is not ready for use in instruction or unfinished in some way. You can add specific details into the Brief Description field above.</p>
              <p>Note that content that is marked as a WIP will be prevented from being listed in the catalogue, but can be listed in your profile.</p>`,
              position: 'right',
              padding: 15
            },
            {
              element: $('#edit-field-show-in-catalogue-wrapper'),
              content: `<h2>Options: Show in Catalogue</h2>
              <p>Keep this option Enabled if you want to make your content find-able in the site <a href="/catalogue" target="_blank">Catalogue</a>. This means that other members of this site can find your content by browsing and searching the catalogue.</p>
              <p>Note that content that is marked as a WIP will be prevented from being listed in the catalogue.</p>`,
              position: 'right',
              padding: 15
            },
            {
              element: $('#edit-field-display-in-profile-wrapper'),
              content: `<h2>Options: Show in Profile</h2>
              <p>Keep this option Enabled if you to feature this content in your <a href="/user" target="_blank">Profile</a>. This means that other members can visit your profile and view the list of content you have selected to be displayed their, like a portfolio of sorts.</p>
              <p>Note that content that is marked as a WIP will still be displayed in your profile.</p>`,
              position: 'right',
              padding: 15
            },
            {
              element: $('#edit-field-h5p-content-0-h5p-content-editor'),
              content: `<h2>H5P Editor</h2>
              <p>This is the H5P Editor panel. You can use this editor to create/edit interactive H5P content.</p>
              <p>If this is your first time using H5P, you should check out the <a href="/pages/getting-started" target="_blank">Getting Started</a> documentation first.`,
              position: 'above',
              padding: 15
            }
          ]);
        });

      }
    }

})(jQuery, Drupal, drupalSettings);